data {
#include data/data.stan
}
transformed data{
  int maxX = max(X);
}
parameters {
  // real<lower = 0> sigma;
  real<lower=0> sigmaloc_loc;
  real<lower=0> sigmaloc_scale;
  vector<lower=-sigmaloc_loc/sigmaloc_scale>[n_sub] sigma_loc_raw;
  real<lower=0> sigma_scale;
  vector<lower = 0>[n_voxel] sigma;

  matrix[n_voxel, 2] meanAngleVector;

  // --- gamma multiplier --
  real<lower = 0> s_gammaloc_loc;
  real<lower = 0> s_gammaloc_scale;
  vector<lower = (-s_gammaloc_loc)/s_gammaloc_scale>[n_sub] s_gamma_loc_raw;

  real<lower = 0> s_gamma_scale;
  vector<lower = 0>[n_voxel] v_gamma;

  // -- concentration --
  real<lower = 0> v_kappa_loc;
  real<lower = 0> v_kappa_scale;
  vector<lower = (-v_kappa_loc)/v_kappa_scale>[n_voxel] v_kappa_raw;

  // -- additive offset
  real s_alphaloc_loc;
  real<lower = 0> s_alphaloc_scale;
  vector<offset = s_alphaloc_loc, multiplier = s_alphaloc_scale>[n_sub] s_alpha_loc;

  real<lower = 0> s_alpha_scale;
  vector[n_voxel] v_alpha;

  // -- NTFP --
  real<lower=ntfp_min> s_ntfploc_loc;
  real<lower=0> s_ntfploc_scale;
  vector<lower=(ntfp_min-s_ntfploc_loc)/s_ntfploc_scale>[n_sub] s_ntfp_loc_raw;

  real<lower = 0> s_ntfp_scale;
  vector<lower=ntfp_min>[n_voxel] v_ntfp;
}
transformed parameters{
  vector<lower=0>[n_sub] s_gamma_loc = s_gammaloc_loc + s_gamma_loc_raw*s_gammaloc_scale;
  vector<lower=0>[n_voxel] v_kappa = v_kappa_loc + v_kappa_raw*v_kappa_scale;
  vector<lower=ntfp_min>[n_sub] s_ntfp_loc = s_ntfploc_loc + s_ntfp_loc_raw*s_ntfploc_scale;
#include tparameters/donut.stan
}
model{
  vector[n_sub] sigma_loc = sigmaloc_loc + sigma_loc_raw*sigmaloc_scale;
  lengthOfMeanAngleVector ~ normal(1, .1);

  sigmaloc_loc ~ gamma(2, priors[1]);
  sigmaloc_scale ~ gamma(2, 0.5);
  sigma_scale ~ gamma(2, 0.5);
  // sigma_loc ~ normal(sigmaloc_loc, sigmaloc_scale);
  sigma_loc_raw ~ std_normal();
  target += -normal_lccdf(-sigmaloc_loc/sigmaloc_scale | 0, 1);

  sigma ~ normal(sigma_loc[sub_by_vox], sigma_scale);
  target += -normal_lccdf(0 | sigma_loc[sub_by_vox], sigma_scale);

  // --- gamma multiplier --
  s_gammaloc_loc ~ normal(0, priors[2]);
  s_gammaloc_scale ~ gamma(priors[3], priors[4]);
  s_gamma_loc_raw ~ std_normal();
  target += -normal_lccdf((-s_gammaloc_loc)/s_gammaloc_scale | 0, 1) * n_sub;

  s_gamma_scale ~ gamma(priors[6], priors[7]);
  v_gamma ~ normal(s_gamma_loc[sub_by_vox], s_gamma_scale);
  target += -normal_lccdf(0 | s_gamma_loc[sub_by_vox], s_gamma_scale);

  // -- concentration --
  v_kappa_loc ~ gamma(priors[8], priors[9]);
  v_kappa_scale ~ gamma(priors[10], priors[11]);
  v_kappa_raw ~ std_normal();
  target += -normal_lccdf((-v_kappa_loc)/v_kappa_scale | 0, 1) * n_voxel;

  // -- additive offset
  s_alphaloc_loc ~ normal(0, priors[12]);
  s_alphaloc_scale ~ gamma(priors[13], priors[14]);
  s_alpha_loc ~ normal(s_alphaloc_loc, s_alphaloc_scale);

  s_alpha_scale ~ gamma(priors[16], priors[17]);
  v_alpha ~ normal(s_alpha_loc[sub_by_vox], s_alpha_scale);

  // -- NTFP --
  s_ntfploc_loc ~ normal(ntfp_min, priors[18]);
  s_ntfploc_scale ~ gamma(priors[19], priors[20]);
  s_ntfp_loc_raw ~ std_normal();
  target += -normal_lccdf((ntfp_min-s_ntfploc_loc)/s_ntfploc_scale | 0, 1) * n_sub;

  s_ntfp_scale ~ gamma(priors[22], priors[23]);
  v_ntfp ~ normal(s_ntfp_loc[sub_by_vox], s_ntfp_scale);
  target += -normal_lccdf(ntfp_min | s_ntfp_loc[sub_by_vox], s_ntfp_scale);

#include model/likelihood.stan
}
