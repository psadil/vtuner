functions{
  real normal_lb_rng(real mu, real sigma, real lb) {
    real p_lb = normal_cdf(lb, mu, sigma); // cdf for lb
    real u = uniform_rng(p_lb, 1);        // unif in bounds
    real y = mu + sigma * Phi(u);        // inverse cdf
    return y;
  }
}
data {
#include /data/data_sbc.stan
}
transformed data {
  // defines alpha, K, sharpening_upper, sharpening_lower
#include /tdata/tdata_sbc.stan
}
parameters {
  // defines sigma, s0, v_weights, v_base_loc, v_base_scale, v_base_raw
#include /parameters/parameters_sbc.stan
  real<lower=0> ntfp_loc;
  real<lower=0> ntfp_scale;
  vector<lower=(1-ntfp_loc)/ntfp_scale>[n_voxel] ntfp_raw;
}
transformed parameters{
  // defines mu, v_base
#include /tparameters/initialize.stan
  vector<lower=0>[n_voxel] ntfp = ntfp_raw*ntfp_scale + ntfp_loc;

  {
    // defines channel_resp_log, channel_resp
#include /tparameters/channel_resp_low.stan

    for(v in 1:n_voxel){
      matrix[n_unique_orientations, n_contrast] resp_to_ori;
      vector[K] vtf0;
      vector[n_obs_per_vox] vtf;
      resp_to_ori[, 1] = channel_resp * v_weights[v];
      resp_to_ori[, 2] = resp_to_ori[, 1] * ntfp[v];
      vtf0 = to_vector(resp_to_ori)*v_base[v];
      vtf = vtf0[X[,v]];
      mu[,v] = vtf - mean(vtf);
    }
  }
}
model{

#include /model/priors.stan

  ntfp_loc ~ normal(0, priors[4]);
  for(v in 1:n_voxel){
    ntfp_raw[v] ~ normal(0, 1) T[(1-ntfp_loc)/ntfp_scale,];
  }
  // likelihood
#include /model/likelihood.stan

}
generated quantities {
  vector[n] y_ = y;
  vector[n_pars] pars_;
  int ranks_[n_pars] = {sigma > sigma_,
                        s0 > s0_,
                        v_base_loc > v_base_loc_,
                        v_base_scale > v_base_scale_,
                        ntfp_loc > ntfp_loc_,
                        ntfp_scale > ntfp_scale_};
  vector[n] log_lik;
  pars_[1] = sigma_;
  pars_[2] = s0_;
  pars_[3] = v_base_loc_;
  pars_[4] = v_base_scale_;
  pars_[5] = ntfp_loc_;
  pars_[6] = ntfp_scale_;
  {
    vector[n] mu0 = to_vector(mu);
    for (i in 1:n) log_lik[i] = normal_lpdf(y[i] | mu0[i], sigma);
  }
}
