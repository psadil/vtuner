data {
#include /data/data.stan
}
transformed data {
  // defines alpha, K, sharpening_upper, sharpening_lower
#include /tdata/tdata.stan
}
parameters {
  // defines sigma, s0, v_weights, v_base_loc, v_base_scale, v_base_raw
#include /parameters/parameters.stan
  real<lower=0> ntfm_loc;
  real<lower=0> ntfm_scale;
  vector<lower=(1-ntfm_loc)/ntfm_scale>[n_voxel] ntfm_raw;
  real<lower=0> ntfa_loc;
  real<lower=0> ntfa_scale;
  vector<lower=(-ntfa_loc)/ntfa_scale>[n_voxel] ntfa_raw;
  real<lower=0> ntfs_loc;
  real<lower=0> ntfs_scale;
  vector<lower=(sharpening_lower-ntfs_loc)/ntfs_scale,
         upper=(sharpening_upper-ntfs_loc)/ntfs_scale>[n_voxel] ntfs_raw;
}
transformed parameters{
  // defines mu, v_base
#include /tparameters/initialize.stan
  vector<lower=1>[n_voxel] ntfm = ntfm_raw*ntfm_scale + ntfm_loc;
  vector<lower=0>[n_voxel] ntfa = ntfa_raw*ntfa_scale + ntfa_loc;
  vector<lower=sharpening_lower, upper=sharpening_upper>[n_voxel] ntfs = ntfs_raw*ntfs_scale + ntfs_loc;

  {
    // defines channel_resp_log, channel_resp
#include /tparameters/channel_resp_low.stan

    for(v in 1:n_voxel){
      matrix[n_channels, n_unique_orientations] channel_resp_log_high;
      matrix[n_unique_orientations, n_channels] channel_resp_high;
      matrix[n_unique_orientations, n_contrast] resp_to_ori;
      vector[K] vtf0;
      vector[n_obs_per_vox] vtf;
      for(ori in 1:n_unique_orientations){
        for(ch in 1:n_channels){
          channel_resp_log_high[ch,ori] = von_mises_lpdf(unique_orientations[ori] | channel_ori[ch], ntfs[v]);
        }
      }
      channel_resp_high = exp(channel_resp_log_high)';

      resp_to_ori[, 1] = channel_resp * v_weights[v];
      resp_to_ori[, 2] = ((channel_resp_high * v_weights[v]) * ntfm[v]) + ntfa[v];
      vtf0 = to_vector(resp_to_ori)*v_base[v];
      vtf = vtf0[X[,v]];
      mu[,v] = vtf - mean(vtf);
    }
  }
}
model{

#include /model/priors.stan

  ntfm_scale ~ gamma(2, priors[5]);
  ntfa_scale ~ gamma(2, priors[5]);
  ntfs_scale ~ gamma(2, priors[5]);

  ntfm_loc ~ normal(0, priors[4]);
  ntfa_loc ~ normal(0, priors[4]);
  ntfs_loc ~ normal(0, priors[4]);
  for(v in 1:n_voxel){
    ntfm_raw[v] ~ normal(0, 1) T[(1-ntfm_loc)/ntfm_scale,];
    ntfa_raw[v] ~ normal(0, 1) T[(-ntfa_loc)/ntfa_scale,];
    ntfs_raw[v] ~ normal(0, 1) T[(sharpening_lower-ntfs_loc)/ntfs_scale,
                                 (sharpening_upper-ntfs_loc)/ntfs_scale];
  }
  // likelihood
#include /model/likelihood.stan

}
