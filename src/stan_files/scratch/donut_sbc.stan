data{
  int n;
}
transformed data{
  real<lower=0> precision_ = exponential_rng(1);
  real meanAngle_ = uniform_rng(-pi(),pi());
  vector[n] y;
  for (i in 1:n) y[i] = von_mises_rng(meanAngle_, precision_);
}
parameters{
  vector[2] meanAngleVector;
  real<lower=0> precision;
}
transformed parameters{
  real lengthOfMeanAngleVector = hypot(meanAngleVector[1], meanAngleVector[2]);
  vector[2] meanAngleUnitVector = meanAngleVector/lengthOfMeanAngleVector;
  real<lower = -pi(), upper = pi()> meanAngle = atan2(meanAngleUnitVector[2], meanAngleUnitVector[1]);
}
model{
  precision ~ exponential(1);
  lengthOfMeanAngleVector ~ normal(1, .1);
  y ~ von_mises(meanAngle, precision);
}
generated quantities{
  vector<lower=-pi(), upper=pi()>[n] y_ = y;
  vector[2] pars_;
  // totally unclear what a rank is for a circular space
  // int ranks_[2] = {fmod(meanAngle - meanAngle_, 2*pi()) > pi(), precision > precision_};
  vector[n] log_lik;
  // int<lower = 0, upper = 1> I_lt_sim[2] = { meanAngle < meanAngle_, precision < precision_ };
  pars_[1] = meanAngle_;
  pars_[2] = precision_;
  for(i in 1:n) log_lik[i] = von_mises_lpdf(y[i] | meanAngle, precision);
}

