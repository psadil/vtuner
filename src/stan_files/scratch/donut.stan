data{
    int n;
    real <lower=-pi(), upper=pi()> measuredAngles[n];
    }
parameters{
    vector[2] meanAngleVector;
    real<lower=0> precision;
}
transformed parameters{
    real lengthOfMeanAngleVector = sqrt(meanAngleVector[1]^2+meanAngleVector[2]^2);
    vector[2] meanAngleUnitVector = meanAngleVector/lengthOfMeanAngleVector;
    real<lower = -pi(), upper = pi()> meanAngle = atan2(meanAngleUnitVector[2], meanAngleUnitVector[1]);
}
model{
    precision~exponential(1);
    lengthOfMeanAngleVector~normal(1,.1);
    measuredAngles~von_mises(meanAngle,precision);
}
