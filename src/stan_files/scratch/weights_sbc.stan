functions{
  real normal_lb_rng(real mu, real sigma, real lb) {
    real p_lb = normal_cdf(lb, mu, sigma); // cdf for lb
    real u = uniform_rng(p_lb, 1);         // unif in bounds
    real y = fma(inv_Phi(u), sigma, mu);       // inverse cdf
    return y;
  }
}
data {
  int n;
  int X[n];
}
transformed data {
  vector[6] priors = [1, 3, 1./2., 2, 1./5., 1./10.]';
  int n_channels = 8;
  int n_unique_orientations = 8;
  vector[n_channels] channel_ori = ([-180, -135, -90, -45, 0, 45, 90, 135]' * pi())/180;
  vector[n_unique_orientations] unique_orientations = ([-180, -135, -90, -45, 0, 45, 90, 135]' * pi())/180;
  vector[n_channels] alpha = rep_vector(1, n_channels);
  vector[n_channels] v_weights0 = dirichlet_rng(alpha);
  real<lower=0> sigma_ = gamma_rng(2, priors[1]);
  real<lower=0> s0_ = gamma_rng(priors[2], priors[3]);
  real<lower=0> v_base_ = normal_lb_rng(0, 1, 0);
  real v_weights1_ = v_weights0[1];
  real v_weights2_ = v_weights0[2];
  real v_weights3_ = v_weights0[3];
  real v_weights4_ = v_weights0[4];
  real v_weights5_ = v_weights0[5];
  real v_weights6_ = v_weights0[6];
  real v_weights7_ = v_weights0[7];
  real v_weights8_ = v_weights0[8];
  vector[n] y;

  {
    // ---- parameters not kept ----
    matrix[n_channels, n_unique_orientations] channel_resp_log_;
    vector[n_unique_orientations] resp_to_ori_;
    vector[n_unique_orientations] vtf0_;
    vector[n] y0;

    for(ori in 1:n_unique_orientations){
      for(ch in 1:n_channels){
        channel_resp_log_[ch,ori] = von_mises_lpdf(unique_orientations[ori] | channel_ori[ch], s0_);
      }
    }
    resp_to_ori_ = exp(channel_resp_log_)' * v_weights0;
    vtf0_ = resp_to_ori_*v_base_;
    y0 = to_vector(normal_rng(vtf0_[X], sigma_));
    y = y0 - mean(y0);
  }
}
parameters {
  simplex[n_channels] v_weights;
  real<lower=0> sigma;
  real<lower=0> s0;
  real<lower=0> v_base;
}
transformed parameters{
  vector[n] mu;
  real v_weights1 = v_weights[1];
  real v_weights2 = v_weights[2];
  real v_weights3 = v_weights[3];
  real v_weights4 = v_weights[4];
  real v_weights5 = v_weights[5];
  real v_weights6 = v_weights[6];
  real v_weights7 = v_weights[7];
  real v_weights8 = v_weights[8];

  {
    matrix[n_channels, n_unique_orientations] channel_resp_log;
    vector[n_unique_orientations] resp_to_ori;
    vector[n_unique_orientations] vtf0;
    vector[n] mu0;

    for(ori in 1:n_unique_orientations){
      for(ch in 1:n_channels){
        channel_resp_log[ch,ori] = von_mises_lpdf(unique_orientations[ori] | channel_ori[ch], s0);
      }
    }
    resp_to_ori = exp(channel_resp_log)' * v_weights;
    vtf0 = resp_to_ori*v_base;
    mu0 = vtf0[X];
    mu = mu0 - mean(mu0);
  }
}
model{
  sigma ~ gamma(2, priors[1]);
  s0 ~ gamma(priors[2], priors[3]);
  v_weights ~ dirichlet(alpha);
  v_base ~ normal(0, 1);
  y ~ normal(mu, sigma);
}
generated quantities {
  vector[n] y_ = y;
  vector[n_channels+3] pars_;
  int ranks_[n_channels+3] = {v_weights1 > v_weights1_,
                            v_weights2 > v_weights2_,
                            v_weights3 > v_weights3_,
                            v_weights4 > v_weights4_,
                            v_weights5 > v_weights5_,
                            v_weights6 > v_weights6_,
                            v_weights7 > v_weights7_,
                            v_weights8 > v_weights8_,
                            sigma > sigma_,
                            s0 > s0_,
                            v_base > v_base_};
  vector[n] log_lik;
  pars_[1] = v_weights1_;
  pars_[2] = v_weights2_;
  pars_[3] = v_weights3_;
  pars_[4] = v_weights4_;
  pars_[5] = v_weights5_;
  pars_[6] = v_weights6_;
  pars_[7] = v_weights7_;
  pars_[8] = v_weights8_;
  pars_[9] = sigma_;
  pars_[10] = s0_;
  pars_[11] = v_base_;
  for (i in 1:n) log_lik[i] = normal_lpdf(y[i] | mu[i], sigma);
}
