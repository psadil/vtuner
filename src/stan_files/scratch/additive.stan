data {
#include /data/data.stan
}
parameters {
  // defines sigma, s0, v_weights, v_base_loc, v_base_scale, v_base_raw
#include /parameters/parameters.stan
}
transformed parameters{
  // defines mu, v_base
#include /tparameters/initialize.stan
  for(v in 1:n_voxel) vtf0[,v] = rep_vector(v_alpha[v], n_unique_orientations);
}
model{
#include /model/priors.stan
#include /model/likelihood.stan
}
