data {
#include /data/data.stan
}
transformed data {
  // defines alpha, K, sharpening_upper, sharpening_lower
#include /tdata/tdata.stan
}
parameters {
  // defines sigma, s0, v_weights, v_base_loc, v_base_scale, v_base_raw
#include /parameters/parameters.stan
  real<lower=0> ntfp_loc;
  real<lower=0> ntfp_scale;
  vector<lower=(sharpening_lower-ntfp_loc)/ntfp_scale,
         upper=(sharpening_upper-ntfp_loc)/ntfp_scale>[n_voxel] ntfp_raw;
}
transformed parameters{
  // defines mu, v_base
#include /tparameters/initialize.stan
  vector<lower=sharpening_lower, upper=sharpening_upper>[n_voxel] ntfp = ntfp_raw*ntfp_scale + ntfp_loc;

  {
    // defines channel_resp_log, channel_resp
#include /tparameters/channel_resp_low.stan

    for(v in 1:n_voxel){
      matrix[n_channels, n_unique_orientations] channel_resp_log_high;
      matrix[n_unique_orientations, n_channels] channel_resp_high;
      matrix[n_unique_orientations, n_contrast] resp_to_ori;
      vector[K] vtf0;
      vector[n_obs_per_vox] vtf;
      for(ori in 1:n_unique_orientations){
        for(ch in 1:n_channels){
          channel_resp_log_high[ch,ori] = von_mises_lpdf(unique_orientations[ori] | channel_ori[ch], ntfp[v]);
        }
      }
      channel_resp_high = exp(channel_resp_log_high)';

      resp_to_ori[, 1] = channel_resp * v_weights[v];
      resp_to_ori[, 2] = channel_resp_high * v_weights[v];
      vtf0 = to_vector(resp_to_ori)*v_base[v];
      vtf = vtf0[X[,v]];
      mu[,v] = vtf - mean(vtf);
    }
  }
}
model{

#include /model/priors.stan

  ntfp_loc ~  normal(1, priors[4]);
  for(v in 1:n_voxel){
    ntfp_raw[v] ~ normal(0, 1) T[(sharpening_lower-ntfp_loc)/ntfp_scale,
                                 (sharpening_upper-ntfp_loc)/ntfp_scale];
  }

  // likelihood
#include /model/likelihood.stan

}
