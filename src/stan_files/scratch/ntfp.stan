data {
  int n;
  int n_voxel; // total voxels across all subs
  int n_sub;
  int sub_by_vox[n_voxel]; // locates the voxel's sub
  int n_unique_orientations;
  vector[23] priors;
  vector[n_unique_orientations] unique_orientations;
  vector[n] y;
  int X[n]; // contains indicator variable (1:(n_contrast*n_voxel*n_sub)) with the trial type on an observation
  int sub[n];
  real<lower = 0> ntfp_min;
  int<lower = 0, upper = 1> modulation;
}
parameters {
  real<lower = 0> sigma;
  matrix[n_voxel, 2] meanAngleVector;

  // --- gamma multiplier --
  real<lower = 0> s_gammaloc_loc;
  real<lower = 0> s_gammaloc_scale;
  vector<lower = (-s_gammaloc_loc)/s_gammaloc_scale>[n_sub] s_gamma_loc_raw;

  real<lower = 0> s_gammascale_loc;
  real<lower = 0> s_gammascale_scale;
  vector<lower = (-s_gammascale_loc)/s_gammascale_scale>[n_sub] s_gamma_scale_raw;

  vector<lower = 0>[n_voxel] v_gamma;

  // -- concentration --
  real<lower = 0> v_kappa_loc;
  real<lower = 0> v_kappa_scale;
  vector<lower = (-v_kappa_loc)/v_kappa_scale>[n_voxel] v_kappa_raw;

  // -- additive offset
  real s_alphaloc_loc;
  real<lower = 0> s_alphaloc_scale;
  vector<offset = s_alphaloc_loc, multiplier = s_alphaloc_scale>[n_sub] s_alpha_loc;

  real<lower = 0> s_alphascale_loc;
  real<lower = 0> s_alphascale_scale;
  vector<lower = (-s_alphascale_loc)/s_alphascale_scale>[n_sub] s_alpha_scale_raw;

  vector[n_voxel] v_alpha;

  // -- NTFP --
  real<lower=ntfp_min> s_ntfploc_loc;
  real<lower=0> s_ntfploc_scale;
  vector<lower=(ntfp_min-s_ntfploc_loc)/s_ntfploc_scale>[n_sub] s_ntfp_loc_raw;

  real<lower = 0> s_ntfpscale_loc;
  real<lower = 0> s_ntfpscale_scale;
  vector<lower=(-s_ntfpscale_loc)/s_ntfpscale_scale>[n_sub] s_ntfp_scale_raw;

  vector<lower=ntfp_min>[n_voxel] v_ntfp;
}
transformed parameters{
  matrix[n_unique_orientations * 2, n_voxel] vtf0;

  vector<lower=0>[n_sub] s_gamma_loc = s_gammaloc_loc + s_gamma_loc_raw*s_gammaloc_scale;
  vector<lower=0>[n_sub] s_gamma_scale = s_gammascale_loc + s_gamma_scale_raw*s_gammascale_scale;

  vector<lower=0>[n_voxel] v_kappa = v_kappa_loc + v_kappa_raw*v_kappa_scale;

  vector<lower = 0>[n_sub]s_alpha_scale = s_alphascale_loc + s_alpha_scale_raw*s_alphascale_scale;

  vector<lower=0>[n_sub] s_ntfp_scale = s_ntfpscale_loc + s_ntfp_scale_raw*s_ntfpscale_scale;
  vector<lower=ntfp_min>[n_sub] s_ntfp_loc = s_ntfploc_loc + s_ntfp_loc_raw*s_ntfploc_scale;

  vector<lower=0>[n_voxel] lengthOfMeanAngleVector = sqrt(rows_dot_self(meanAngleVector));
  matrix[n_voxel, 2] meanAngleUnitVector = append_col(meanAngleVector[,1] ./ lengthOfMeanAngleVector,
  meanAngleVector[,2] ./ lengthOfMeanAngleVector);
  vector<lower = -pi(), upper = pi()>[n_voxel] meanAngle;

  for(v in 1:n_voxel) meanAngle[v] = atan2(meanAngleUnitVector[v,2], meanAngleUnitVector[v,1]);

  for(v in 1:n_voxel){
      vector[n_unique_orientations] resp_to_ori_log;
      vector[n_unique_orientations] resp_to_ori;
      for(ori in 1:n_unique_orientations){
        resp_to_ori_log[ori] = von_mises_lpdf(unique_orientations[ori] | meanAngle[v], v_kappa[v]);
      }
      resp_to_ori = exp(resp_to_ori_log)*v_gamma[v];
      if(modulation==0){
        vtf0[,v] = v_alpha[v] + append_row(resp_to_ori, resp_to_ori + v_ntfp[v]);
      }else if(modulation==1){
        vtf0[,v] = v_alpha[v] + append_row(resp_to_ori, resp_to_ori * v_ntfp[v]);
      }
    }
}
model{
  sigma ~ gamma(3, priors[1]);
  lengthOfMeanAngleVector ~ normal(1, .1);

  // --- gamma multiplier --
  s_gammaloc_loc ~ normal(0, priors[2]);
  s_gammaloc_scale ~ gamma(priors[3], priors[4]);
  s_gamma_loc_raw ~ std_normal();
  target += -normal_lccdf((-s_gammaloc_loc)/s_gammaloc_scale | 0, 1) * n_sub;

  s_gammascale_loc ~ normal(0, priors[5]);
  s_gammascale_scale ~ gamma(priors[6], priors[7]);
  s_gamma_scale_raw ~ std_normal();
  target += -normal_lccdf((-s_gammascale_loc)/s_gammascale_scale | 0, 1) * n_sub;

  v_gamma ~ normal(s_gamma_loc[sub_by_vox], s_gamma_scale[sub_by_vox]);
  target += -normal_lccdf(0 | s_gamma_loc[sub_by_vox], s_gamma_scale[sub_by_vox]);

  // -- concentration --
  v_kappa_loc ~ gamma(priors[8], priors[9]);
  v_kappa_scale ~ gamma(priors[10], priors[11]);
  v_kappa_raw ~ std_normal();
  target += -normal_lccdf((-v_kappa_loc)/v_kappa_scale | 0, 1) * n_voxel;

  // -- additive offset
  s_alphaloc_loc ~ normal(0, priors[12]);
  s_alphaloc_scale ~ gamma(priors[13], priors[14]);
  s_alpha_loc ~ normal(s_alphaloc_loc, s_alphaloc_scale);

  s_alphascale_loc ~ normal(0, priors[15]);
  s_alphascale_scale ~ gamma(priors[16], priors[17]);
  s_alpha_scale_raw ~ std_normal();
  target += -normal_lccdf((-s_alphascale_loc)/s_alphascale_scale | 0, 1) * n_sub;

  v_alpha ~ normal(s_alpha_loc[sub_by_vox], s_alpha_scale[sub_by_vox]);

  // -- NTFP --
  s_ntfploc_loc ~ normal(ntfp_min, priors[18]);
  s_ntfploc_scale ~ gamma(priors[19], priors[20]);
  s_ntfp_loc_raw ~ std_normal();
  target += -normal_lccdf((ntfp_min-s_ntfploc_loc)/s_ntfploc_scale | 0, 1) * n_sub;

  s_ntfpscale_loc ~ normal(0, priors[21]);
  s_ntfpscale_scale ~ gamma(priors[22], priors[23]);
  s_ntfp_scale_raw ~ std_normal();
  target += -normal_lccdf((-s_ntfpscale_loc)/s_ntfpscale_scale | 0, 1) * n_sub;

  v_ntfp ~ normal(s_ntfp_loc[sub_by_vox], s_ntfp_scale[sub_by_vox]);
  target += -normal_lccdf(ntfp_min | s_ntfp_loc[sub_by_vox], s_ntfp_scale[sub_by_vox]);

  {
    vector[n_unique_orientations * 2 * n_voxel] vtf = to_vector(vtf0);
    y ~ normal(vtf[X], sigma);
  }
}
