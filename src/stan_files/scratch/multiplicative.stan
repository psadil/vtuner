data {
  int n;
  int n_voxel; // total voxels across all subs
  int n_sub;
  int sub_by_vox[n_voxel]; // locates the voxel's sub
  int n_unique_orientations;
  vector[14] priors;
  vector[n_unique_orientations] unique_orientations;
  vector[n] y;
  int X[n]; // contains indicator variable (1:(n_contrast*n_voxel*n_sub)) with the trial type on an observation
  int sub[n];
  real<lower=0> ntfp_min;
}
parameters {
  // defines sigma, s0, v_weights, v_base_loc, v_base_scale, v_base_raw
#include /parameters/parameters.stan

}
transformed parameters{
  matrix[n_unique_orientations, n_voxel] vtf0;

  vector<lower=0>[n_voxel] v_kappa = v_kappa_loc + v_kappa_raw*v_kappa_scale;

  vector<lower=0>[n_sub] s_gamma_loc = s_gammaloc_loc + s_gamma_loc_raw*s_gammaloc_scale;
  vector<lower=0>[n_sub] s_gamma_scale = s_gammascale_loc + s_gamma_scale_raw*s_gammascale_scale;

  vector<lower = 0>[n_sub]s_alpha_loc = s_alphaloc_loc + s_alpha_loc_raw*s_alphaloc_scale;
  vector<lower = 0>[n_sub]s_alpha_scale = s_alphascale_loc + s_alpha_scale_raw*s_alphascale_scale;

  vector<lower=0>[n_sub] s_ntfp_scale = s_ntfpscale_loc + s_ntfp_scale_raw*s_ntfpscale_scale;
  vector<lower=ntfp_min>[n_sub] s_ntfp_loc = s_ntfploc_loc + s_ntfploc_raw*s_ntfploc_scale;

  vector<lower=0>[n_voxel] lengthOfMeanAngleVector = sqrt(rows_dot_self(meanAngleVector));
  matrix[n_voxel, 2] meanAngleUnitVector = append_col(meanAngleVector[,1] ./ lengthOfMeanAngleVector,
                                                      meanAngleVector[,2] ./ lengthOfMeanAngleVector);
  vector<lower = -pi(), upper = pi()>[n_voxel] meanAngle;

  for(v in 1:n_voxel) meanAngle[v] = atan2(meanAngleUnitVector[v,2], meanAngleUnitVector[v,1]);

  {
    for(v in 1:n_voxel){
      vector[n_unique_orientations] resp_to_ori_log;
      vector[n_unique_orientations] resp_to_ori;
      for(ori in 1:n_unique_orientations){
         resp_to_ori_log[ori] = von_mises_lpdf(unique_orientations[ori] | meanAngle[v], v_kappa[v]);
      }
      resp_to_ori = exp(resp_to_ori_log)*v_base[v];
      if(modulation==0){
        vtf0[,v] = v_int[v] + append_row(resp_to_ori, resp_to_ori*ntfp[v]);
      }else if(modulation==1){
        vtf0[,v] = v_int[v] + append_row(resp_to_ori, resp_to_ori+ntfp[v]);
      }
    }
  }
}
model{
#include /model/priors.stan
// ntfp_scale ~ gamma(priors[2], priors[3]);

#include /model/likelihood.stan
}
