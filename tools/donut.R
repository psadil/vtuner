library(rstan)
library(tidyverse)
library(CircStats)
options(mc.cores = parallel::detectCores()-1)


#' @param model precompiled Stan model
#' @param data data for model (defaults to empty list)
#'
#' @return size of the generated quantity array I_lt_sim
num_monitored_params <- function(model, data = list()) {
  fit <- sampling(model, data = data,
                  iter = 1, chains = 1, warmup = 0,
                  refresh = 0, seed = 1234)
  fit@par_dims$I_lt_sim
}

#' @param y sequence of ranks in 1:max_rank
#' @param max_rank maximum rank of data in y
#' @param bins (default 20): bins to use for chi-square test
#' @error return NA if max rank not divisible by number of bins
#'
#' @return p-value for chi-square test that data is evenly
#' distributed among the bins
test_uniform_ranks <- function(y, max_rank, bins = 20) {
  if (max_rank / bins != floor(max_rank / bins)) {
    # printf("ERROR in test_uniform_ranks")
    # printf(" max rank must be divisible by bins.")
    # printf(" found max rank = %d; bins = %d", max_rank, bins)
    return(NA)
  }
  bin_size <- max_rank / bins
  bin_count <- rep(0, bins)
  N <- length(y)
  for (n in 1:N) {
    bin <- ceiling(y[n] / bin_size)
    bin_count[bin] <- bin_count[bin] + 1
  }
  chisq.test(bin_count)$p.value
}

#' @param model precompiled Stan model
# #' @param data: list of data for model (defaults to empty)
#' @param sbc_sims number of total simulation runs for SBC
#' @param stan_sims number of posterior draws per Stan simulation
#' @param init_thin initial thinning (doubles thereafter up to max)
#' @param max_thin max thinning level
#' @param seed PRNG seed to use for Stan program to generate data
#' @param target_n_eff target effective sample size (should be 80%
#' or 90% of stan_sims to stand a chance)
#'
#' @return list with keys (rank, p_value, thin) for 2D array of ranks
#' and 1D array of p-values, and 1D array of thinning rates
sbc_parallel_antithetic <- function(model,
                                    # data = list(),
                                    sbc_sims = 1000,
                                    stan_sims = 999,
                                    init_thin = 4,
                                    max_thin = 64,
                                    n_eff_reltol=0.2) {
  # num_params <- num_monitored_params(model, data)
  num_params <- 2
  ranks <- matrix(nrow = sbc_sims, ncol = num_params)
  thins <- rep(NA, sbc_sims)
  sbc_sim <- function(n) {
    seed <- seq(from = 0, to = .Machine$integer.max, length.out = sbc_sims)[n]
    n_eff <- 0
    thin <- init_thin
    ranks <- rep(NA,num_params) #matrix(nrow = sbc_sims, ncol = num_params)
    while (TRUE) {
      x <- 100
      meanAngle_ <- runif(1, max=2*pi)
      precision <- rexp(1, 1)

      data0 <- list(y = CircStats::rvm(x,meanAngle_,precision) - pi,
                    n = x,
                    precision_ = precision,
                    meanAngle_ = meanAngle_ - pi)

      fit <- sampling(
        model,
        data = data0,
        seed = seed,
        chains = 1,
        iter = 2 * thin * stan_sims,
        thin = thin,
        control = list(adapt_delta = 0.9),
        refresh = 0)
      fit_summary <- summary(fit, pars = c("lp__"), probs = c())$summary
      n_eff <- fit_summary["lp__", "n_eff"]
      if ((n_eff/stan_sims >= 1-n_eff_reltol && n_eff/stan_sims <= 1+n_eff_reltol) || (2*thin) > max_thin) break;
      thin <- 2 * thin
    }

    lt_sim <- rstan::extract(fit)$I_lt_sim
    for (i in 1:num_params)ranks[i] <- sum(lt_sim[ , i]) + 1
    c(thin,ranks)
  }
  rslt <- parallel::mclapply(1:sbc_sims, sbc_sim)
  for (n in 1:sbc_sims) {
    thins[n] = rslt[[n]][1]
    ranks[n,] = rslt[[n]][-1]
  }
  list(rank = ranks, thin = thins)
}

sbcmodel <- rstan::stan_model("tools/donut.stan")

sbc_sims <- 100 # N
stan_sims <- (2^10) - 1 # L
out <- sbc_parallel_antithetic(
  sbcmodel,
  sbc_sims = sbc_sims,
  stan_sims = stan_sims,
  init_thin = 4,
  max_thin = 64,
  n_eff_reltol = 0.2
)

L <- stan_sims
Lp1 <- L + 1
N <- dim(out$rank)[1]
groups <- 2^6
bins <- Lp1 / groups

ranks <- tibble(
  col = 1:2,
  parameter = c("mu","kappa"))  %>%
  mutate(rank = map(col, ~tibble(rank = out$rank[,.x]))) %>%
  select(-col)

pvals <- ranks %>%
  mutate(p = map_dbl(rank, ~test_uniform_ranks(y=.x$rank, max_rank=Lp1, bins=bins))) %>%
  select(-rank) %>%
  mutate(p = round(p, digits = 3))

ribs <- ranks %>%
  select(-rank) %>%
  mutate(
    ymin = qbinom(0.005, size = N, prob = 1/bins),
    ymax = qbinom(0.995, size = N, prob = 1/bins)) %>%
  crossing(rank = 1:L)

ranks %>%
  unnest(cols = rank) %>%
  ggplot(aes(x = rank)) +
  facet_wrap(~parameter) +
  geom_histogram(breaks = seq(0, stan_sims, length.out = bins+1)) +
  geom_ribbon(
    aes(ymin = ymin, ymax=ymax),
    alpha = .3,
    data=ribs) +
  geom_label(
    aes(label = p),
    x = 20,
    y = 20,
    data = pvals)
