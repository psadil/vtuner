data{
  int n;
  real meanAngle_;
  real precision_;
  vector<lower=-pi(), upper=pi()>[n] y;
}
parameters{
  vector[2] meanAngleVector;
  real<lower=0> precision;
}
transformed parameters{
  real lengthOfMeanAngleVector = hypot(meanAngleVector[1], meanAngleVector[2]);
  vector[2] meanAngleUnitVector = meanAngleVector/lengthOfMeanAngleVector;
  real<lower = -pi(), upper = pi()> meanAngle = atan2(meanAngleUnitVector[2], meanAngleUnitVector[1]);
}
model{
  precision ~ exponential(1);
  lengthOfMeanAngleVector ~ normal(1, .1);
  y ~ von_mises(meanAngle, precision);
}
generated quantities{
  int<lower = 0, upper = 1> I_lt_sim[2] = {meanAngle < meanAngle_, precision < precision_};
}

