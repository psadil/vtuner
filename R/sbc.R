#' Simulatio-Based Calibration
#'
#' @param M Int
#' @param priors a
#' @param n_channels a
#' @param n_voxel  a
#' @param n_contrast a
#' @param n_unique_orientations a
#' @param n_runs a
#' @param ... a
#'
#' @details Largely a wrapper around [rstan::sbc()].
#'
#' @export
sbc <- function(M,
                priors = c(1, 3, 1/2, 2, 1/5, 1/10),
                n_voxel = 10,
                n_contrast = 2,
                n_unique_orientations = 8,
                n_channels = 8,
                n_runs = 2,
                ...) {
  standata <- make_sbc_data(priors = priors,
                            n_voxel = n_voxel,
                            n_contrast = n_contrast,
                            n_unique_orientations = n_unique_orientations,
                            n_channels = n_channels,
                            n_runs = n_runs)

  output <- rstan::sbc(stanmodels[["ntf_sbc"]],
                       data = standata,
                       M = M,
                       ...)

  return(output)
}

#' @param priors a
#' @param n_channels a
#' @param n_voxel  a
#' @param n_contrast a
#' @param n_unique_orientations a
#' @param n_runs a
#'
#' @export
#' @describeIn sbc get data
make_sbc_data <- function(priors,
                          n_voxel,
                          n_contrast,
                          n_unique_orientations,
                          n_channels,
                          n_runs){

  unique_orientations <- seq(-pi,
                             pi-(2*pi/n_unique_orientations),
                             length.out = n_unique_orientations)
  channel_ori <- unique_orientations

  n_obs_per_vox <- n_unique_orientations * n_contrast * n_runs
  n <- n_obs_per_vox * n_voxel
  X <- matrix(rep(1:(n_unique_orientations * n_contrast),
                  times = n_voxel,
                  each = n_runs),
              ncol = n_voxel)

  standata <- list(n = n,
                   n_voxel = n_voxel,
                   n_contrast = n_contrast,
                   n_unique_orientations = n_unique_orientations,
                   n_channels = n_channels,
                   priors = priors,
                   unique_orientations = unique_orientations,
                   channel_ori = channel_ori,
                   n_obs_per_vox = n_obs_per_vox,
                   X = X)

  return(standata)
}


#' @export
#' @describeIn sbc useful
get_parnames <- function(model = "ntf_sbc"){

  stanmodel <- stanmodels[[model]]

  # parameter names
  stan_code <- rstan::get_stancode(stanmodel)
  stan_code <- scan(what = character(), sep = "\n", quiet = TRUE, text = stan_code)
  pars_lines <- grep("[[:space:]]*(pars_)|(pars_\\[.*\\])[[:space:]]*=", stan_code, value = TRUE)
  pars_lines <- pars_lines[!grepl("^[[:space:]]*vector", pars_lines) &
                             !grepl("^[[:space:]]*real", pars_lines)]
  pars_names <- trimws(sapply(strsplit(pars_lines, split = "=", fixed = TRUE), tail, n = 1))
  pars_names <- unique(sub("^([a-z,A-Z,0-9,_]*)_.*;", "\\1", pars_names))
  return(pars_names)
}

#' @export
#' @describeIn sbc useful
postprocess_sbc <- function(post, pars_names){

  # divergences, etc.
  sampler_params <- simplify2array(sapply(post,
                                          FUN = rstan::get_sampler_params,
                                          inc_warmup = FALSE))

  # prior predictive distribution
  Y <- sapply(post, FUN = function(p) {
    means <- rstan::get_posterior_mean(p)[, 1]
    # will be just 'y_' if length 1, otherwise will have brackets
    means[grepl("y_$|y_\\[.*\\]", names(means))]
  })

  # realizations of true parameters from transformed data
  pars <- sapply(post, FUN = function(p) {
    means <- rstan::get_posterior_mean(p)[, 1]
    mark <- grepl("pars_\\[[[:digit:]]+\\]", names(means))
    return(means[mark])
  })
  if (length(pars) > 0L) {
    if (is.null(dim(pars))) pars <- matrix(pars, nrow=1)
    if (dim(pars)[1] != length(pars_names)) {
      warning("parameter names miscalculated due to non-compliance with conventions; see help(sbc)")
      pars_names <- NULL
    }
    if (is.null(dim(pars))) {
      pars <- t(as.matrix(pars))
    }
    rownames(pars) <- pars_names
  }

  # not actually ranks but rather unthinned binary values for draw > true
  ranks <- lapply(post, FUN = function(p) {
    r <- rstan::extract(p, pars = "ranks_", permuted = FALSE)[, 1, ]
    if (is.null(dim(r))) {
      r <- as.matrix(r)
    }
    colnames(r) <- pars_names
    r[] <- r > 0
    return(r)
  })

  pareto_k <- sapply(post,
                     FUN = function(x) suppressWarnings(rstan::loo(x))$diagnostics$pareto_k)

  out <- list(ranks = ranks,
              Y = Y,
              pars = pars,
              sampler_params = sampler_params,
              pareto_k = pareto_k)
  class(out) <- "sbc"
  return(out)
}
