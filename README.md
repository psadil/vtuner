
<!-- README.md is generated from README.Rmd. Please edit that file -->

# vtuner

<!-- badges: start -->
<!-- badges: end -->

## Installation

vtuner relies on the RStan interface to [Stan](https://mc-stan.org). To
install vtuner, first [follow instructions for setting up
RStan](https://github.com/stan-dev/rstan/wiki/RStan-Getting-Started).

After successfully installing RStan, vtuner can be installed with
devtools

``` r
# install.packages(devtools)
library(devtools)
devtools::install_gitlab("psadil/vtuner")
```

# Example Analysis

## Data

A sample dataset is provided with this package. The dataset contains the
beta values for a single participant, and shows the format expected by
the functions of this package. The dataset can be loaded with the
following command.

``` r
# library(vtuner)
data("sub02")
knitr::kable(head(sub02))
```

| sub | run | voxel  | contrast | orientation |         y | ses |
|:----|:----|:-------|:---------|------------:|----------:|:----|
| 2   | 15  | 191852 | low      |   0.7853982 |  3.359860 | 3   |
| 2   | 15  | 197706 | low      |   0.7853982 | -2.839522 | 3   |
| 2   | 15  | 197769 | low      |   0.7853982 | -2.027267 | 3   |
| 2   | 15  | 197842 | low      |   0.7853982 |  2.234859 | 3   |
| 2   | 15  | 197906 | low      |   0.7853982 |  2.858387 | 3   |
| 2   | 15  | 197907 | low      |   0.7853982 |  1.506754 | 3   |

For extra info on the dataset, see the help page for betas (?betas).

## Run Stan

The technique works by comparing separate models, each of which allows
just a single kind of modulation to the neural tuning functions. The two
kinds of neuromodulation currently implemented are *Additive* and
*Multiplicative*. Source for the models can be found on this [package’s
repository](https://gitlab.com/psadil/vtuner/tree/master/src/stan_files).
The models are largely the same, but allow for different forms of
neuromodulation.

The main functions of this package are `run_stan()`, which outputs a
`VtuneFit` model object. That object has a `loo` method which can be
used to estimate a leave-one-out cross-validation score. The READMM
walks through an example analysis.

### Define Stan options

The function `run_stan` will be used to fit the Bayesian model. The
`RStan` backend provides many options for configuring how the model
runs. See `?rstan::stan` for further details. Here, we set a few of
those parameters.

``` r
#' number of chains to run
chains <- 2
#' number of cores to run the chains on
#' this value would run each chain on a separate core
cores <- 2

#' seed for reproducible results!
seed <- 1234

#' number of posterior samples will be iter - warmup
warmup <- 1000
iter <- 1500

#' these parameters slow the sampling procedure down from the default
#' values in rstan, but are often required on these models
max_treedepth <- 12
adapt_delta <- 0.99

# parameters that will not be saved in the output
pars <- c("meanAngleVector", "s_gamma_loc_raw", "s_gamma_scale_raw",
          "v_kappa_raw", "s_alpha_loc_raw", "s_alpha_scale_raw",
          "s_ntfp_loc_raw", "s_ntfp_scale_raw","vtf0",
          "lengthOfMeanAngleVector", "meanAngleUnitVector")
```

Next, define a helper function for specifying the priors. This function
outputs a vector that can be used by `run_stan()`.

``` r
set_priors <- function(modulation){

  if(modulation == "multiplicative"){
    priors <- c(1/2, # sigma ~ gamma(2, priors[1]);
                5, # s_gammaloc_loc ~ normal(0, priors[2]);
                2, 1, # s_gammaloc_scale ~ gamma(priors[3], priors[4]);
                5, # s_gammascale_loc ~ normal(0, priors[5]); -- UNUSED
                2, 1/20, # s_gamma_scale ~ gamma(priors[6], priors[7]);
                3, 1, # v_kappa_loc ~ gamma(priors[8], priors[9]);
                3, 1, # v_kappa_scale ~ gamma(pri ors[10], priors[11]);
                5, # s_alphaloc_loc ~ normal(0, priors[12]);
                2, 1/2, # s_alphaloc_scale ~ gamma(priors[13], priors[14]);
                5, # s_alphascale_loc ~ normal(0, priors[15]);  -- UNUSED
                2, 1/2, # s_alpha_scale ~ gamma(priors[16], priors[17]);
                0.5, # s_ntfploc_loc ~ normal(ntfp_min, priors[18]);
                2, 3, # s_ntfploc_scale ~ gamma(priors[19], priors[20]);
                2, # s_ntfpscale_loc ~ normal(0, priors[21]);  -- UNUSED
                2, 3 # s_ntfp_scale ~ gamma(priors[22], priors[23]);
    )
  }else{
    priors <- c(1/2, # sigma ~ gamma(2, priors[1]);
                5, # s_gammaloc_loc ~ normal(0, priors[2]);
                2, 1, # s_gammaloc_scale ~ gamma(priors[3], priors[4]);
                5, # s_gammascale_loc ~ normal(0, priors[5]); -- UNUSED
                2, 1/20, # s_gamma_scale ~ gamma(priors[6], priors[7]);
                3, 1, # v_kappa_loc ~ gamma(priors[8], priors[9]);
                3, 1, # v_kappa_scale ~ gamma(pri ors[10], priors[11]);
                5, # s_alphaloc_loc ~ normal(0, priors[12]);
                2, 1/2, # s_alphaloc_scale ~ gamma(priors[13], priors[14]);
                5, # s_alphascale_loc ~ normal(0, priors[15]);  -- UNUSED
                2, 1/2, # s_alpha_scale ~ gamma(priors[16], priors[17]);
                5, # s_ntfploc_loc ~ normal(ntfp_min, priors[18]);
                2, 1/2, # s_ntfploc_scale ~ gamma(priors[19], priors[20]);
                5, # s_ntfpscale_loc ~ normal(0, priors[21]);  -- UNUSED
                2, 1/2 # s_ntfp_scale ~ gamma(priors[22], priors[23]);
    )
  }
  return(priors)
}
```

### Models

In addition to the different forms of neuromodulation, there are two
different models to choose from. Both models estimate parameters
hierarchically across voxels. One model, `"vtf"`, nests voxels by
participant, adding another layer in the hierarchy. The other model,
`"vtf0"`, treats avoids that extra layer of hierarchy and treats all
voxels equally. This example works with the latter, simpler version.

Note that running even one chain may take a few hours.

The commands to run the two models are similar. The main differences are
1) the priors on the neuromodulation parameter (determined by the above
function, `set_prior()`), and 2) the value given to the `model` argument
in \`run_stan()\`\`.

### Multiplicative

``` r
fitm <- run_stan(
  d = sub02,
  priors = set_priors("multiplicative"),
  control = list(
    adapt_delta = adapt_delta,
    max_treedepth = max_treedepth),
  warmup = warmup,
  iter = iter,
  chains = chains,
  cores = cores,
  pars = pars,
  include = FALSE,
  model = "multiplicative",
  smodel = "vtf0",
  seed = seed)
```

### Additive

``` r
fita <- run_stan(
  d = sub02,
  priors = set_priors("additive"),
  control = list(
    adapt_delta = adapt_delta,
    max_treedepth = max_treedepth),
  warmup = warmup,
  iter = iter,
  chains = chains,
  cores = cores,
  pars = pars,
  include = FALSE,
  model = "additive",
  smodel = "vtf0",
  seed = seed)
```

## Model Comparison

The outputs of `run_stan()` can be used for model comparison. This
package comes equipped with functions for estimating the leave-one-out
cross validation score, using a function called `loo()`.

``` r
m <- loo(fitm)
a <- loo(fita)
```

The outputs of these functions are `loo::loo` objects, which can be
passed directly to the `loo` package functions like `loo_compare()`.

``` r
loo::loo_compare(m, a)
```

Please see the `loo` package and documentation for further details.
